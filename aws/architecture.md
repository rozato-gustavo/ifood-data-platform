![architecture](architecture.png)

Como o modelo específico é rapidamente treinado, para maior controle de gastos e para tirar a necessidade de treinar o mesmo
modelo diversas vezes quando alguma imagem docker (que faz predição) é escalada, o treinamento irá ocorrer dentro de uma função lambda em qual
irá salvar o modelo dentro do S3 para manter consistência entra as instâncias que irão utiliza-lo. O trigger para treinamento
(junto com possíveis novos parametros) pode ser realizado através de um endpoint mapeado pelo Api Gateway.

O fluxo de predição começa com uma request chegando no serviço Api Gateway para que sejam feitas as verificações de parametros e
versionamento para a entrada de possíveis novos modelos no futuro.

Um ELB é responsável por direcionar o tráfego entre os containeres contidos no ECS (o qual busca suas imagens no ECR). 
O ECS tem o trabalho de escalar esses containeres de acordo com algum target (uso de CPU, por exemplo). Esses containeres possuem a 
aplicação flask rodando em que faz a previsão. Ao iniciar um novo container para predição, o mesmo faz uma chamada para o modelo no S3, carrega
o modelo no container e assim está preparado para fazer predições.

OBS: Como essa era uma parte mais aberta do desáfio, tentei adaptar um pouco mais o problema em questão para uma arquitetura em núvem,
como por exemplo: retirando a lógica de treinamento dentro da mesma imagem docker.
