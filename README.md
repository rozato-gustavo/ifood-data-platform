# ifood-data-platform
Para fazer predições, primeiro é necessário construir sua imagem docker, a qual irá treinar o modelo:

```docker build -t iris_classification:latest .```

Para fazer o deploy da imagem em um container, execute:

```docker run -p 5000:5000 iris_classification:latest```

Flask SwaggerUI server vai estar rodadno em: 0.0.0.0:5000

É possível testar a aplicação por meio de uma chamada GET, junto com os 4 query params:

curl -X GET "http://0.0.0.0:5000/iris_classification/?sepal_length=3&sepal_width=3&petal_length=3&petal_width=3" -H "accept: application/json"

A arquitetura da segunda etapa para o problema em uma cloud AWS está dentro da pasta /aws
